# Event and Listener

## Event

> _Something that has happened in our app that we can respond to._
>
> For example, fs.readStream emits an event when the file is opened.

## Event Listener

> _The code that responds to an event._ The listener code primarily is a function.

## Emitter

> Emitter are actually objects with properties set as array of functions, which are invoked as listener functions, when that event is called/emitted.

Create our own Emitter class by function constructor.
```javascript
function Emitter() {
  this.events = {};
}

/**
 * Create new events
 * @param {String} type 
 * @param {Function CallBack} listener 
 */
Emitter.prototype.on = function(type, listener) {
  this.events[type] = this.events[type] || [];
  this.events[type].push(listener);
}

/**
 * Responds to the particular event.type
 * @param {String} type 
 */
Emitter.prototype.emit = function(type) {
  if(this.events[type]) {
    this.events[type].forEach(function(listener) {
      listener();
    });
  }
}

module.exports = Emitter;
```

*File*   : basicEmitter.js
*app.js* : simulating file of basicEmitter.js

## Using Node Events

Node comes with inbuilt class of events. Inherting any other class from this, will make the extending class access to 'on', 'emit' prototype methods.

Checkout the program for simple synchronous and asynchronous way to read a file.

```javascript
var Emitter = require('events'); // events module
```

Replicating the same our own Emitter object with Node events module.
```javascript
var emt1 = new Emitter();

// adding new emitter type greet with a listener
emt1.on('greet', function() {
  console.log('Hi Bro!');
});

// appending another listener to the greet event type
emt1.on('greet', function() {
  console.log('Wassup!');
});

// adding new emitter type welcome
emt1.on('welcome', function() {
  console.log('Welcome to the Jungle!');
});

// listening to the greet event
emt1.emit('greet');

// listening to the welcome event
emt1.emit('welcome');
```

*File* : usingNodeEvents.js

## Inheriting events

> Any object extending or inherting events module can easily create it's own events(.on) and listen to them(.emit).

```javascript
var Emitter = require('events');
var util = require('util');

function Welcome() {
  this.greet = function() {
    console.log('Greetings!');
  }
}

// using util to inherit the Emitter to Welcome
util.inherits(Welcome, Emitter);

var w1 = new Welcome();

// possible due to the inheritance of Emitter
w1.on('greet', function() {
  console.log('Hola!');
})

w1.emit('greet');

w1.greet(); // constructor functions's greet method
```
*File* : inhertingEvents.js