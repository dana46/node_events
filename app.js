var Emitter = require('./basicEmitter');

var emt1 = new Emitter();

// adding new emitter type greet with a listener
emt1.on('greet', function() {
  console.log('Hi Bro!');
});

// appending another listener to the greet event type
emt1.on('greet', function() {
  console.log('Wassup!');
});

// adding new emitter type welcome
emt1.on('welcome', function() {
  console.log('Welcome to the Jungle!');
});

// listening to the greet event
console.log('=============== GREET EMITTER ===============');
emt1.emit('greet');

// listening to the welcome event
console.log('============== WELCOME EMITTER ===============');
emt1.emit('welcome');

