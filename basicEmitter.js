function Emitter() {
  this.events = {};
}

/**
 * Create new events
 * @param {String} type 
 * @param {Function CallBack} listener 
 */
Emitter.prototype.on = function(type, listener) {
  this.events[type] = this.events[type] || [];
  this.events[type].push(listener);
}

/**
 * Responds to the particular event.type
 * @param {String} type 
 */
Emitter.prototype.emit = function(type) {
  if(this.events[type]) {
    this.events[type].forEach(function(listener) {
      listener();
    });
  }
}

module.exports = Emitter;