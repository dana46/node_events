var Emitter = require('events');
var util = require('util');

function Welcome() {
  this.greet = function() {
    console.log('Greetings!');
  }
}

// using util to inherit the Emitter to Welcome
util.inherits(Welcome, Emitter);

var w1 = new Welcome();

// possible due to the inheritance of Emitter
w1.on('greet', function() {
  console.log('Hola!');
})

w1.emit('greet');

w1.greet(); // constructor functions's greet method